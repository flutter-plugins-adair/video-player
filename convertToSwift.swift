import AVFoundation
import GLKit

func FLTCMTimeToMillis(_ time: CMTime) -> nt64_t {
    if time.timescale == 0 {
        return 0
    }
    return CMTimeScale(time.value * 1000) / time.timescale
}

class FLTFrameUpdater: NSObject {
    var textureId: Int64 = 0
    private(set) weak var registry: (NSObjectProtocol & FlutterTextureRegistry)?

    func on(_ link: CADisplayLink?) {
        registry?.textureFrameAvailable(textureId)
    }

    convenience init?(registry: (NSObjectProtocol & FlutterTextureRegistry)?) {
        assert(self != nil, "super init cannot be nil")
        self.registry = registry
    }
}

class FLTVideoPlayer: NSObject, FlutterTexture, FlutterStreamHandler {
    private(set) var player: AVPlayer?
    private(set) var videoOutput: AVPlayerItemVideoOutput?
    private(set) var displayLink: CADisplayLink?
    var eventChannel: FlutterEventChannel?
    var eventSink: FlutterEventSink?
    var preferredTransform: CGAffineTransform!
    private(set) var disposed = false
    private(set) var isPlaying = false
    var isLooping = false
    private(set) var isInitialized = false

    init(url: URL?, frameUpdater: FLTFrameUpdater?) {
    }

    func play() {
    }

    func pause() {
    }

    func setIsLooping(_ isLooping: Bool) {
    }

    func updatePlayingState() {
    }
}

//  The converted code is limited to 2 KB.
//  Upgrade your plan to remove this limitation.
// 
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
class FLTVideoPlayer {
    convenience init(asset: String?, frameUpdater: FLTFrameUpdater?) {
        let path = Bundle.main.path(forResource: asset, ofType: nil)
        self.init(url: URL(fileURLWithPath: path ?? ""), frameUpdater: frameUpdater)
    }

    func addObservers(_ item: AVPlayerItem?) {
        item?.addObserver(self, forKeyPath: "loadedTimeRanges", options: [.initial, .new], context: &timeRangeContext)
        item?.addObserver(self, forKeyPath: "status", options: [.initial, .new], context: &statusContext)
        item?.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: [.initial, .new], context: &playbackLikelyToKeepUpContext)
        item?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: [.initial, .new], context: &playbackBufferEmptyContext)
        item?.addObserver(self, forKeyPath: "playbackBufferFull", options: [.initial, .new], context: &playbackBufferFullContext)

        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: OperationQueue.main, using: { note in
            if self.isLooping {
                let p = note?.object as? AVPlayerItem
                p?.seek(to: .zero, completionHandler: nil)
            } else {
                if self.eventSink {
                    self.eventSink([
                    "event": "completed"
                    ])
                }
            }
        })
    }

    @inline(__always) private func radiansToDegrees(_ radians: CGFloat) -> CGFloat {
        // Input range [-pi, pi] or [-180, 180]
        let degrees = GLKMathRadiansToDegrees(Float(radians))
        if degrees < 0 {
            // Convert -90 to 270 and -180 to 180
            return degrees + 360
        }
        // Output degrees in between [0, 360[
        return degrees
    }

    //  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
    func getVideoComposition(with transform: CGAffineTransform, with asset: AVAsset?, withVideoTrack videoTrack: AVAssetTrack?) -> AVMutableVideoComposition? {
        let instruction = AVMutableVideoCompositionInstruction.videoCompositionInstruction as? AVMutableVideoCompositionInstruction
        instruction?.timeRange = CMTimeRangeMake(CMTime.zero, asset?.duration)
        var layerInstruction: AVMutableVideoCompositionLayerInstruction? = nil
        if let videoTrack = videoTrack {
            layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        }
        layerInstruction?.setTransform(preferredTransform, at: .zero)

        let videoComposition = AVMutableVideoComposition.videoComposition as? AVMutableVideoComposition
        instruction?.layerInstructions = [layerInstruction].compactMap { $0 }
        videoComposition?.instructions = [instruction].compactMap { $0 }

        // If in portrait mode, switch the width and height of the video
        var width = videoTrack?.naturalSize.width
        var height = videoTrack?.naturalSize.height
        let rotationDegrees = Int(round(radiansToDegrees(atan2(preferredTransform.b, preferredTransform.a))))
        if rotationDegrees == 90 || rotationDegrees == 270 {
            width = videoTrack?.naturalSize.height
            height = videoTrack?.naturalSize.width
        }
        videoComposition?.renderSize = CGSize(width: width ?? 0.0, height: height ?? 0.0)

        videoComposition?.frameDuration = CMTimeMake(1, 30)

        return videoComposition
    }


}